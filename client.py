import sys
from socket import *

#Main
if __name__ == '__main__':
    # Check that we have 2 arguments
    if(len(sys.argv) != 3):
	    print "Invalid arguments."
	    sys.quit()
    # Grabs args
    serverName = sys.argv[1] 
    serverPort = int(sys.argv[2])
    # Creates connection with server
    clientSocket = socket(AF_INET,SOCK_STREAM)
    clientSocket.connect((serverName,serverPort))
    # Loops around (add protocol here)
    while 1:
        sentence = raw_input('Enter input: ')
        input = sentence.split(" ")
        if (input[0] == 'help'):
            print 'The following commands are supported: \nput NAME VALUE TYPE\nget NAME TYPE\ndel NAME TYPE\nbrowse\nexit'
        elif (input[0] == 'type'):
            clientSocket.send(sentence)
            response = clientSocket.recv(1024)
            print response
        elif (input[0] == 'put'):
			#check proper formatting of input
			if (len(input) != 4):
			    print 'Incorrect formatting. Try Again.'
			else:
				#formatting ok. enter into file
				clientSocket.send(sentence)
				#get server response
				response = clientSocket.recv(1024)
				check = response.split(" ")
				if check[0] == 'failed':
					print 'Entry could not be added\n'
				else:
					print 'Succeful insertion of entry\n'
        elif (input[0] == 'get'):
            #check proper formatting of input
			if (len(input) != 3):
			    print 'Incorrect formatting. Try Again.'
			else:
				#formatting ok. enter into file
				clientSocket.send(sentence)
				#server response
				response = clientSocket.recv(1024)
				#check to see if failed of success
				check = response.split(" ")
				if check[0] == 'failed':
					print 'Does not exist\n'
				else:
					print 'got entry: ' + response + '\n'
        elif (input[0] == 'del'):
            #check proper formatting of input
			if (len(input) != 3):
			    print 'Incorrect formatting. Try Again.'
			else:
				#formatting ok. enter into file
				clientSocket.send(sentence)
				#get server response
				response = clientSocket.recv(1024)
				#check to see if failed of success
				check = response.split(" ")
				if check[0] == 'failed':
					print 'Entry does not exist\n'
				else:
					print 'deleted entry: ' + sentence + '\n'
        elif (input[0] == 'browse'):
			clientSocket.send('browse')
			#get server response
			more_data = 1
			while (more_data == 1):
				clientSocket.settimeout(5)
				response = clientSocket.recv(1024)
				check = response.split(" ")
				if check[0] == 'SHIT':
					more_data = 0
				else:
					print response + '\n'
        elif (input[0] == 'exit'):
			#close connection and exit
			clientSocket.close()
			exit()
        else:
			print 'command ' + input[0] + ' not recognized. Type help for usage information.\n'
